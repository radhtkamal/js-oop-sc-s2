//FUNCTION CODING:

//Translate the other students specifics into their own respective objects. Make sure to include the login, logout, and listGrades method in each object, as well.

/*
Name: Joe
Email: joe@mail.com
Grades: 78, 82, 79, 85

Name: Jane
Email: jane@mail.com
Grades: 87, 89, 91, 93

Name: Jessie
Email: jessie@mail.com
Grades: 91, 89, 92, 93
*/

let studentOne = {
  name: 'John',
  email: 'john@mail.com',
  grades: [89, 84, 78, 88],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  }
}

let studentTwo = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  }
}

let studentThree = {
  name: 'Jane',
  email: 'jane@mail.com',
  grades: [87, 89, 91, 93],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  }
}

let studentFour = {
  name: 'Jessie',
  email: 'jessie@mail.com',
  grades: [91, 89, 92, 93],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  }
}

//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4).

let studentTwoA = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
  averageGrade() {
    console.log(`${this.name}'s total grade average is ${(this.grades[0]+this.grades[1]+this.grades[2]+this.grades[3])/4}`);
  }
}

//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

let studentTwoB = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],
  avgGrade: (this.grades[0]+this.grades[1]+this.grades[2]+this.grades[3])/4,
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
  averageGrade() {
    console.log(`${this.name}'s total grade average is ${avgGrade}`);
  },
  willPass() {
    if (avgGrade >= 85) {
      return true;
    } else {
      return false;
    }
  }
}

//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

let studentOneFinal = {
  name: 'John',
  email: 'john@mail.com',
  grades: [89, 84, 78, 88],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
  averageGrade() {
    console.log(`${this.name}'s total grade average is ${avgGrade}`);
  },
  willPass() {
    if (this.avgGrade >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    if (this.avgGrade >= 90 ) {
      return true;
    } else if ((this.avgGrade < 90) && (this.avgGrade >= 85)){
      return false;      
    } else {
      return undefined;
    }
  }
}

let studentTwoFinal = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],
  avgGrade: (this.grades[0]+this.grades[1]+this.grades[2]+this.grades[3])/4,
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
  averageGrade() {
    console.log(`${this.name}'s total grade average is ${avgGrade}`);
  },
  willPass() {
    if (this.avgGrade >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    if (this.avgGrade >= 90 ) {
      return true;
    } else if ((this.avgGrade < 90) && (this.avgGrade >= 85)){
      return false;      
    } else {
      return undefined;
    }
  }
}

let studentThreeFinal = {
  name: 'Jane',
  email: 'jane@mail.com',
  grades: [87, 89, 91, 93],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
  averageGrade() {
    console.log(`${this.name}'s total grade average is ${avgGrade}`);
  },
  willPass() {
    if (this.avgGrade >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    if (this.avgGrade >= 90 ) {
      return true;
    } else if ((this.avgGrade < 90) && (this.avgGrade >= 85)){
      return false;      
    } else {
      return undefined;
    }
  }
}

let studentFourFinal = {
  name: 'Jessie',
  email: 'jessie@mail.com',
  grades: [91, 89, 92, 93],
  login() {
    console.log(`${this.email} has logged in.`);
  },
  logout() {
    console.log(`${this.email} has logged out.`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
  },
  averageGrade() {
    console.log(`${this.name}'s total grade average is ${avgGrade}`);
  },
  willPass() {
    if (this.avgGrade >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    if (this.avgGrade >= 90 ) {
      return true;
    } else if ((this.avgGrade < 90) && (this.avgGrade >= 85)){
      return false;      
    } else {
      return undefined;
    }
  }
}



//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
let clOf1A = {
  students: [studentOneFinal, studentTwoFinal, studentThreeFinal, studentFourFinal]
}

//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

let claOf1A = {
  students: [studentOneFinal, studentTwoFinal, studentThreeFinal, studentFourFinal],
  countHonorStudents() {
    let honorsCount = [];
    this.students.forEach(student => {
      if (student.willPassWithHonors() == true) {
        honorsCount.push(student);
      } 
    })
    return honorsCount.length;
  }
}

//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

let clasOf1A = {
  students: [studentOneFinal, studentTwoFinal, studentThreeFinal, studentFourFinal],
  countHonorStudents() {
    let honorsCount = [];
    this.students.forEach(student => {
      if (student.willPassWithHonors() == true) {
        honorsCount.push(student);
      } 
    })
    return honorsCount.length;
  },
  honorsPercentage() {
    let count = this.countHonorStudents();
    let percentage = (count/4) * 100;
    return percentage;
  }
}


//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

let classOf1A = {
  students: [studentOneFinal, studentTwoFinal, studentThreeFinal, studentFourFinal],
  countHonorStudents() {
    let honorsCount = this.students.map(student => {
      if (student.willPassWithHonors() == true) {
        honorsCount.push(student);
      } 
    })
    return honorsCount.length;
  },
  honorsPercentage() {
    let count = this.countHonorStudents();
    let percentage = (count/4) * 100;
    return percentage;
  },
  retrieveHonorStudentInfo() {
    let honorsCount = this.students.map(student => {
      if (student.willPassWithHonors() == true) {
        honorsCount.push(student);
      } 
    });
    
    let honorsInfo = honorsCount.map (student => {
      return {
        studEmail: student.email,
        studAveGr: student.avgGrade
      }
    });

    return honorsInfo;
  }
}


//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.